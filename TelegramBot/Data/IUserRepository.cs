﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot
{
    internal interface IUserRepository
    {
        Task<List<UserModel>> GetAllAsync();
        Task<UserModel> GetUser(long id);
        Task AddUser(UserModel user);
        Task DeleteUser(long id);
        Task UpdateFormAsync(long id, string form);
        Task Update(UserModel user);
    }
}
