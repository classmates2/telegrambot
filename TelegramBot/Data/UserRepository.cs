﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot
{
    internal class UserRepository : IUserRepository
    {
        private UserContext _context = new UserContext();
        public async Task AddUser(UserModel user)
        {
            using var transaction = _context.Database.BeginTransaction();
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            transaction.Commit();
        }

        public async Task DeleteUser(long id)
        {
            var user = await GetUser(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }
        public Task<List<UserModel>> GetAllAsync()
        {
            return _context.Users.ToListAsync();
        }

        public  async Task<UserModel> GetUser(long id)
        {
            return await  _context.Users.FirstOrDefaultAsync(user => user.ChatId == id);
        }

        public async Task Update(UserModel user)
        {
            _context.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFormAsync(long id, string form)
        {
            using var transaction = _context.Database.BeginTransaction();
            var user = await GetUser(id);
            user.Form = form;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            transaction.Commit();
        }
    }
}
