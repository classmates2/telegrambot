﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.Enums;

namespace TelegramBot
{
    internal static class HandleMessager
    {
        public static Dictionary<UserStates, Func<TelegramBotClient, Update, IUserRepository, UserModel,Task>> HandleMessages { get; private set; } = new Dictionary<UserStates, Func<TelegramBotClient, Update, IUserRepository, UserModel,Task>>();
        static HandleMessager()
        {
            HandleMessages.Add(UserStates.New, async (TelegramBotClient client, Update update, IUserRepository userRepository, UserModel user) =>
            {
                await client.SendTextMessageAsync(user.ChatId, "Привет SoulMatebbot это бот для для анонимного общения \n Напиши о себе в одном сообщении,это позволит другим пользователям и вам быстрее найти собеседника");
                user.State = UserStates.Unregistered;
            });
            HandleMessages.Add(UserStates.Unregistered, async (TelegramBotClient client, Update update, IUserRepository userRepository, UserModel user) =>
            {
                await userRepository.UpdateFormAsync(user.ChatId, update.Message.Text);
                await client.SendTextMessageAsync(user.ChatId, "Начнем искать собеседника?\n1.Да\n2.Нет");
                user.State = UserStates.Inactive;
            });
            HandleMessages.Add(UserStates.Inactive, async (TelegramBotClient client, Update update, IUserRepository userRepository, UserModel user) =>
            {
                switch (update.Message.Text)
                {
                    case "1":
                        await client.SendTextMessageAsync(user.ChatId, "поиск начат");
                        user.State = UserStates.Active;
                        break;
                    case "2":
                        await client.SendTextMessageAsync(user.ChatId, "напиши,как захочешь найти нового собеседника");
                        break;
                    default:
                        await client.SendTextMessageAsync(user.ChatId, "введены неверный данные\nвведите \n1.Да\n2.Нет");
                        break;
                }
            });
            HandleMessages.Add(UserStates.Active, async (TelegramBotClient client, Update update, IUserRepository userRepository, UserModel user) =>
            {
                await client.SendTextMessageAsync(user.ChatId, "идет поиск собеседника");
            });
            HandleMessages.Add(UserStates.InChat, async (TelegramBotClient client, Update update, IUserRepository userRepository, UserModel user) =>
            {
                switch (update.Message.Text)
                {
                    case "/end":
                        user.State = UserStates.Inactive;
                        await client.SendTextMessageAsync(user.ChatId, "Начнем искать собеседника?\n1.Да\n2.Нет");
                        break;
                    default:
                        await client.SendTextMessageAsync(user.CompanionId, update.Message.Text);
                        break;
                }
            });
        }
    }
}
