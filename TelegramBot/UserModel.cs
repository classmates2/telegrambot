﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.Enums;
namespace TelegramBot
{
    internal class UserModel
    {
        public UserStates State { get; set; } =UserStates.New;
        public string Form { get; set; } = "";
        public int Straiks { get; set; } = 0;
        [Key]
        public long ChatId { get; set; }
        public long CompanionId { get; set; }
        public async  Task HandleMessage (TelegramBotClient client, Update update, IUserRepository userRepository)
        {
          await HandleMessager.HandleMessages[State](client,update,userRepository,this);
        }
    }
}
