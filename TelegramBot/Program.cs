﻿global using Microsoft.EntityFrameworkCore;
global using Telegram.Bot;
global using Telegram.Bot.Types;
using TelegramBot;
using TelegramBot.Enums;

class Program
{
   static object locker = new();
    public static async Task Main()
    {
        string token = "5393046208:AAEZoQ27IFs39rCIriwXRW9BcHX6LqedTwU";
        Console.WriteLine("start");
        TelegramBotClient client = new(token);
        await Start(client);
       
    }
    public static async Task Start(TelegramBotClient client)
    {
        int offset = 1;
        while (true)
        {
            UserRepository userRepository = new UserRepository();
            UserModel user = null!;
            Update[] updates = await client.GetUpdatesAsync(offset: offset);
            UserModel user1 = null;
            UserModel user2 = null;
            List<UserModel> activeUsers;
            activeUsers = (await userRepository.GetAllAsync()).Where(x => x.State == UserStates.Active).ToList();
            foreach (var item in activeUsers)
            {
                if (user1 != null)
                {
                    user1 = item;
                    continue;
                }
                if (user2 != null)
                {
                    user2 = item;
                    continue;
                }
                await client.SendTextMessageAsync(user1.ChatId, "Собеседник наден");
                await client.SendTextMessageAsync(user2.ChatId, "Собеседник наден");
                user1.CompanionId = user2.ChatId;
                user1.State = UserStates.InChat;
                user2.CompanionId = user1.ChatId;
                user2.State = UserStates.InChat;
                user1 = null;
                user2 = null;
            }
            foreach (Update update in updates)
            {
                var id = update.Message.Chat.Id;
                Console.WriteLine(update.Message.Text);
                if (await userRepository.GetUser(id) == null)
                {
                    user = new UserModel() { ChatId = id };
                    await userRepository.AddUser(user);

                }
                else
                {
                    user = await userRepository.GetUser(id);
                }
                Console.WriteLine(user.State);
                user.HandleMessage(client, update, userRepository);
              await  userRepository.Update(user);
                offset = update.Id + 1;
            }
        }
    }
}
